import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterUser } from './../_models/registerUser';
import { UtilsService } from '../_services/utils.service';
import { MainService } from './../_services/main.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  user: RegisterUser = {
    name: '',
    email: '',
    password: '',
    id: 0,
  };
  constructor(private fb: FormBuilder,
    private util: UtilsService,
    private mainService: MainService, private router: Router) {
    this.registerForm = this.fb.group({
      'id': [this.user.id],
      'name': [this.user.name, [Validators.required, Validators.minLength(2)]],
      'email': [this.user.email, [Validators.required, Validators.email]],
      'password': [this.user.password, [Validators.required, Validators.minLength(6)]]
    });
  }


  registerUser(): void {
    if (!this.registerForm.valid) {

      if (this.registerForm.get('name').hasError('required') ||
        this.registerForm.get('email').hasError('required') ||
        this.registerForm.get('password').hasError('required')) {
        this.util.showNotify('please fill in the blanks', 'danger');
      }else
      {
        this.util.showNotify('please fill in the blanks with appropriate info', 'danger');
        return
      }
    }
    else {
      this.registerForm.value.id = Math.floor(Math.random() * 100);
    
      const result = this.mainService.registerUser(this.registerForm.value);
      if (result) {
        this.util.showNotify('You registered successfully', 'success');
        this.registerForm.setValue({
          id: 0,
          name: '',
          email: '',
          password: ''
        });
        this.router.navigate(['/']);
      }
      else {
        this.util.showNotify('This email is exist', 'danger');
      }
    }
  }





  ngOnInit(): void {

  }

}
