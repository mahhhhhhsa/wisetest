import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegisterUser } from './../_models/registerUser';


@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private http: HttpClient) { }

  registerUser(user: RegisterUser) {

    let list = JSON.parse(localStorage.getItem('userList'));
    if (list) {
      // exist email //
      console.log(user);
      const findUser = list.findIndex(item => item.email === user.email);
      console.log(findUser);

      if (findUser !== -1) {
        console.log(findUser);
        return false;
      }
      // create new user // 
      else {
        list.push(user);
      }

    }
    else {
      list = [];
      list.push(user);
    }

    localStorage.setItem('userList', JSON.stringify(list));

    return true;

  }

  loginUser(userr) {

    const list = JSON.parse(localStorage.getItem('userList'));

    if (list) {
      const findUser = list.find(user => user.email === userr.email &&
        user.password === userr.password);

      if (findUser) {
        const userData = {
          name: findUser.name,
          email: findUser.email
        }
        const json = JSON.stringify(userData);
        const encodeToken = btoa(json);
        localStorage.setItem("token", encodeToken);
        return true;
      } else {
        return false
      }


    }
    else {
      return false
    }

  }
}
