import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../_services/utils.service';

declare var Chart: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  chart = [];
  myDoughnutChart = [];
  line = [];

  constructor(private utils: UtilsService) { }

  ngOnInit(): void {
    // pie chart initial
    this.showPiechart();
    //bar chart initial
    this.showBarchart();
    // line chart initial
    this.showLine();
    // count uniquie visitors
    this.utils.countUp(100, '.uniqueVisitors');
    // count new orders 
    this.utils.countUp(50, '.newOrders');
 // count total customers 
 this.utils.countUp(400, '.totalCustomers');

    


  }

  showPiechart() {


    const canvasPie = <HTMLCanvasElement>document.getElementById('pie');
    const ctxx = canvasPie.getContext('2d');

    this.myDoughnutChart = new Chart(ctxx, {
      type: 'doughnut',
      data: {
        labels: ['Visitors', 'Onlines', 'Sales'],
        datasets: [{
          label: '# of Votes',
          data: [100, 150, 70],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',



          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',


          ],
          borderWidth: 0.1
        }]
      },
      options: {
        legend: {
          display: true
        },
        cutoutPercentage: 60,
        rotation: 1 * Math.PI,
        circumference: 1 * Math.PI
      }


    });


  }

  showBarchart() {
    const canvas = <HTMLCanvasElement>document.getElementById('chart');
    const ctx = canvas.getContext('2d');

    this.chart = new Chart(ctx, {

      type: 'bar',
      data: {
        labels: ['October', 'Novamber', 'July', 'December', 'March', 'May'],
        datasets: [{
          label: '# of Orders 2019',
          data: [20, 10, 8, 30, 15, 40],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',


          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',

          ],
          borderWidth: 1
        }, {
          label: '# of Sales 2019',
          data: [10, 15, 20, 30, 5, 10],
          backgroundColor: [
            'rgba(54, 162, 235, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(54, 162, 235, 0.2)',

          ],
          borderColor: [
            'rgba(54, 162, 235, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(54, 162, 235, 1)',
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

  }

  showLine() {
    const canvas = <HTMLCanvasElement>document.getElementById('line');
    const ctx = canvas.getContext('2d');

    this.line = new Chart(ctx, {

      type: 'line',
      data: {
        labels: ['October', 'Novamber', 'July', 'December', 'March', 'May'],
        datasets: [{
          label: '# of Price Growth 2019',
          data: [5, 20, 1, 20, 10, 35],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',

          ],
        }, {
          label: '# of Orders Growth 2019',
          data: [25, 10, 1, 10, 20, 15],
          backgroundColor: [
            'rgba(255, 159, 64, 0.2)'

          ],
        }]
      },
      options: {
        fill: false,
        showLine: true
      }
    });

  }

}
