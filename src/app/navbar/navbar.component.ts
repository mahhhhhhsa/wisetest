import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  username = '';

  constructor(private router : Router) { }

  ngOnInit(): void {

    this.getUserName();
  }

  getUserName():void{
    let token = localStorage.getItem('token');
    let decode = atob(token);
    let decodeToken = JSON.parse(decode);
    this.username =decodeToken.name;
   
  }

  logout():void{
    localStorage.removeItem('token');
    this.router.navigate(['/']);
  }

}
