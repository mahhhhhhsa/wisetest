import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilsService } from '../_services/utils.service';
import { Router } from '@angular/router';
import { MainService } from '../_services/main.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  user = {
    email: '',
    password: '',
  };

  constructor(private fb: FormBuilder,
    private util: UtilsService,
    private mainService: MainService, private router: Router) {
    this.loginForm = this.fb.group({
      'email': [this.user.email, [Validators.required, Validators.email]],
      'password': [this.user.password, [Validators.required, Validators.minLength(6)]]
    });
  }

  ngOnInit(): void {
  }


  login(): void {
    if (!this.loginForm.valid) {

      if (this.loginForm.get('email').hasError('required') ||
        this.loginForm.get('password').hasError('required')) {
        this.util.showNotify('please fill in the blanks', 'danger');
      } else {
        this.util.showNotify('please fill in the blanks with appropriate info', 'danger');
        return
      }
    }
    else {
      const result = this.mainService.loginUser(this.loginForm.value);
      console.log(result);
      if(result){
        this.util.showNotify('You loginned successfully', 'success');
        this.router.navigate(['/dashboard']);
      }else{
        this.util.showNotify('Incorrect email or password', 'danger');
      }
    }

  }

}
