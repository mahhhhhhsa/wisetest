import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'wiseTest';
  isnavbar = false;

  constructor(private router: Router) {
    let token = localStorage.getItem('token');
    if(!token){
      router.navigate(['/']);
    }
    router.events.subscribe(val => {

      this.isnavbar =
     
          this.router.url.startsWith('/dashboard')
        
          ? true
          : false;
    });
   

  }

  ngOnInit(): void {

    


  }





}
